<!DOCTYPE html>
<html>
<head>
<title>Libro de Recetas</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" type="text/css" href="recursos/css/index.css">
</head>
<body>

<!-- Seccion de barra de navegacion -->
<div class="w3-top">
  <div class="w3-bar w3-white w3-padding w3-card" id="nav" >
    <a href="index.php" class="w3-bar-item w3-button">El Libro de Recetas</a>
    
    <div class="w3-right w3-hide-small">
      <a href="vistas/recetas-caseras/recetas-caseras.html" class="w3-bar-item w3-button">Recetas Caseras</a>
      <a href="vistas/recetas-postres/recetas-postres.html" class="w3-bar-item w3-button">Recetas Postres</a>
      <a href="index.php#contact" class="w3-bar-item w3-button">Contactenos</a>
      <a href="#" class="w3-bar-item w3-button" onclick="document.getElementById('login').style.display='block'">Iniciar Sesión</a>
    </div>
  </div>
</div>

<!-- Seccion de cabecera -->
<header class="w3-display-container w3-content w3-wide" id="home">
  <img class="w3-image" src="recursos/imagenes/background.jpg" alt="Hamburger Catering" width="1600" height="800">
  <div class="w3-display-bottomleft w3-padding-large w3-opacity">
    <h1 class="w3-xxlarge">Catering</h1>
  </div>
</header>

<!-- Seccion de Pagina de contenido -->
<div id="contenido" class="w3-content" >
<!-- se muestra recetas mas populares -->
<br>
<hr><h1 class="w3-center">Recetas mas Populares</h1><br><hr>
  <!-- primera previa receta -->
  <div class="w3-row w3-padding-64" id="about">
    <div class="w3-col m6 w3-padding-large w3-hide-small">
     <img src="recursos/imagenes/salmon-marinado.jpg" class="w3-round w3-image w3-opacity-min" alt="Table Setting" width="600" height="750">
    </div>

    <div class="w3-col m6 w3-padding-large">
      <h1 class="w3-center">Salmon Marinado</h1><br>
      <h5 class="w3-center">Ingredientes para 6 personas</h5>
      <p class="w3-large">
        - 1 cucharada de aceite de oliva
        - 500 gramos de azúcar
        - 20 gramos de eneldo
        - 1 cucharada de miel
        - 1 cucharadita de mostaza marrón
      </p>
      <p class="w3-large w3-text-grey w3-hide-medium">A un salmón fresco, limpio y sin cabeza se le quita la raspa central rociándolo con eneldo fresco. Por cada kilo de salmón se pone encima medio kilo de sal gorda y medio kilo de azúcar mezclado todo. Se pone sobre papel de aluminio y se aplasta un poco. Se mete en la nevera de dos a cuatro días. Se saca y se limpia del azúcar y de la sal.
        <a href="vistas/recetas-caseras/salmon-marinado.html"><span class="w3-tag w3-light-grey">Ver mas...</span> </a>
      </p>
    </div>
  </div>
  <hr>

  <!-- segunda previa receta -->
  <div class="w3-row w3-padding-64" id="about">
    <div class="w3-col m6 w3-padding-large w3-hide-small">
     <img src="recursos/imagenes/rollo-de-carne.jpg" class="w3-round w3-image w3-opacity-min" alt="Table Setting" width="600" height="750">
    </div>

    <div class="w3-col m6 w3-padding-large">
      <h1 class="w3-center">Rollo de Carne</h1><br>
      <h5 class="w3-center">Ingredientes para 8 personas</h5>
      <p class="w3-large">
        - 1 kilo de carne picada de cerdo
        - 1 kilo de carne picada de ternera
        - 70 ccs de coñac
        - 4 huevos
        - 150 gramos de jamón picado
      </p>
      <p class="w3-large w3-text-grey w3-hide-medium">Se mezclan las carnes y se van añadiendo los huevos de uno en uno, amasándolo todo. Se añade el resto de ingredientes y se mezclan bien hasta formar una masa. 
        <a href="vistas/recetas-caseras/rollo-de-carne.html"><span class="w3-tag w3-light-grey">Ver mas...</span> </a>
      </p>
    </div>
  </div>
  <hr>
  
  <!-- tercera previa receta -->
  <div class="w3-row w3-padding-64" id="about">
    <div class="w3-col m6 w3-padding-large w3-hide-small">
     <img src="recursos/imagenes/berenjenas-rellenas.jpg" class="w3-round w3-image w3-opacity-min" alt="Table Setting" width="600" height="750">
    </div>

    <div class="w3-col m6 w3-padding-large">
      <h1 class="w3-center">Berenjenas Rellenas</h1><br>
      <h5 class="w3-center">Ingredientes para 4 personas</h5>
      <p class="w3-large">
        - aceite de oliva
        - 4 berenjenas
        - 3 cebollas picadas
        - 150 gramos de champiñones
        - 2 huevo, yemas
      </p>
      <p class="w3-large w3-text-grey w3-hide-medium">Cortar a lo largo por la mitad las berenjenas y vaciarlas con cuidado.           Picar la carne de las berenjenas y rehogarla con las cebollas en una sartén con aceite.Cuando estén doraditas, agregar los champiñones, perejil y la miga de pan. Rehogar todo bien hasta obtener un relleno de consistencia.  
        <a href="vistas/recetas-caseras/berenjenas-rellenas.html"><span class="w3-tag w3-light-grey">Ver mas...</span> </a>
      </p>
    </div>
  </div>
  <hr>

  <!-- cuarta previa receta -->
  <div class="w3-row w3-padding-64" id="about">
    <div class="w3-col m6 w3-padding-large w3-hide-small">
     <img src="recursos/imagenes/tiramisu.jpg" class="w3-round w3-image w3-opacity-min" alt="Table Setting" width="600" height="750">
    </div>

    <div class="w3-col m6 w3-padding-large">
      <h1 class="w3-center">Postre Tiramisú</h1><br>
      <h5 class="w3-center">Ingredientes para 8 personas</h5>
      <p class="w3-large">
        - 0,5 vasos de azúcar
        - 24 bizcochos blandos (de soletilla)
        - 100 gramos de cacao puro en polvo
        - 1 tazón de café solo
      </p>
      <p class="w3-large w3-text-grey w3-hide-medium">Echar la mitad del azúcar en la nata líquida y montarla hasta que quede muy compacta (debe estar fría y a ser posible en un recipiente metálico frío). Separar las claras y las yemas de los huevos, montar las claras a punto de nieve (mejor si se echan unas gotas de limón o vinagre) Mezclar despacio con un cucharón el queso mascarpone y las yemas de huevo, añadiendo el resto del azúcar (no utlizar la batidora).   
        <a href="vistas/recetas-postres/tiramisu.html"><span class="w3-tag w3-light-grey">Ver mas...</span> </a>
      </p>
    </div>
  </div>
  <hr>
  <br>

  <form class="w3-container" method="GET" action="index.php#tweet" id="tweet">
    <span>
      <img src="recursos/imagenes/Twitter.png" width="30" height="30">
      <h2 class="w3-left">Buscar Tweets</h2>
      <input class="w3-input" type="text" name="tweetName" placeholder="Digite nombre de usuario Twitter sin @">
    </span>
    <button type="submit" class="w3-button w3-black">Buscar</button>
</form>


  <?php 


  include('clases/showTweets.php');
  ?>

  <!-- Seccion de contactenos -->
  <div class="w3-container w3-padding-64" id="contact">
    <h1>Contactenos</h1><br>
    <p>Nos place seguir ayudando a que sea un profesional en la cocina</p>

    <form action="/action_page.php" target="_blank">
      <p><input class="w3-input w3-padding-16" type="text" placeholder="Nombre" required name="Name"></p>
      <p><input class="w3-input w3-padding-16" type="email" placeholder="Correo" required name="Correo"></p>
      <p><input class="w3-input w3-padding-16" type="datetime-local" placeholder="Date and time" required name="date" value="2018-04-26T20:00"></p>
      <p><input class="w3-input w3-padding-16" type="text" placeholder="Mensaje u Obervaciones" required name="Message"></p>
      <p><button class="w3-button w3-light-grey w3-section" type="submit">Enviar Informacion</button></p>
    </form>
  </div>

  <div class="w3-container">
  <div id="login" class="w3-modal">
    <div class="w3-modal-content w3-card-4 w3-animate-zoom" style="max-width:600px">

      <div class="w3-center"><br>
        <img src="recursos/imagenes/usuario.png" alt="Avatar" style="width:30%" class="w3-circle w3-margin-top">
      </div>

      <form class="w3-container" action="/action_page.php">
        <div class="w3-section">
          <label><b>Nombre de Usuario</b></label>
          <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Ingrese Nombre de Usuario" name="username" required>
          <label><b>Contraseña</b></label>
          <input class="w3-input w3-border" type="password" placeholder="Ingrese Contraseña" name="psw" required>
          <button class="w3-button w3-block w3-green w3-section w3-padding" type="submit">Login</button>
          <button class="w3-button w3-block w3-green w3-section w3-padding" type="submit">Iniciar con Google</button>
          <input class="w3-check w3-margin-top" type="checkbox" checked="checked"> Recuerdame
        </div>
      </form>

      <div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
        <button onclick="document.getElementById('login').style.display='none'" type="button" class="w3-button w3-red">Cancelar</button>
        <span class="w3-right w3-padding w3-hide-small">Olvidaste la <a href="#">contraseña?</a></span>
      </div>

    </div>
  </div>
</div>
  

</div>

</body>
</html>
